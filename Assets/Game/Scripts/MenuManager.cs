using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    enum Screen
    {
        None,
        Main,
        Setting,
        Levels
    }

    public CanvasGroup mainScreen;
    public CanvasGroup settingsScreen;
    public CanvasGroup levelsScreen;

    private void Start()
    {
        SetCurrentScreen(Screen.Main);
    }

    private void SetCurrentScreen(Screen screen)
    {
        Utility.SetCanvasGroupEnabled(mainScreen, screen == Screen.Main);
        Utility.SetCanvasGroupEnabled(settingsScreen, screen == Screen.Setting);
        Utility.SetCanvasGroupEnabled(levelsScreen, screen == Screen.Levels);
    }

    public void StartNewGame(int lvl)
    {
        SetCurrentScreen(Screen.None);
        SceneManager.LoadScene(lvl);
    }

    public void OpenSettings()
    {
        SetCurrentScreen(Screen.Setting);
    }

    public void OpenLevels()
    {
        SetCurrentScreen(Screen.Levels);
    }

    public void CloseSettings()
    {
        SetCurrentScreen(Screen.Main);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
